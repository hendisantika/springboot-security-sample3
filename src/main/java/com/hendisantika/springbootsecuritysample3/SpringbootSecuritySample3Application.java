package com.hendisantika.springbootsecuritysample3;

import com.hendisantika.springbootsecuritysample3.entity.Role;
import com.hendisantika.springbootsecuritysample3.entity.User;
import com.hendisantika.springbootsecuritysample3.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;

@Slf4j
@SpringBootApplication
public class SpringbootSecuritySample3Application {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSecuritySample3Application.class, args);
    }

    @PostConstruct
    public void init() {
        User user = new User();
        user.setUsername("admin");
        user.setEmail("admin@hendisantika.com");
        user.setPassword(passwordEncoder.encode("123456"));
        user.setEnabled(true);
        user.setRole(Role.ADMIN);
        userRepository.save(user);
    }
}
