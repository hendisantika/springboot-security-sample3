package com.hendisantika.springbootsecuritysample3.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-sample3
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/09/18
 * Time: 20.48
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@Controller
public class AdminCsrfTestController {

    @GetMapping("/admin")
    public String users(Model model) {

        String xss = "<script>\n" +
                "alert('This can happen if \\'XSS\\' is not blocked well.\\n\\ntoken: ' + token + '\\nheader: ' + header);\n" +
                "$.post('/user/email', 'email=csrf@xss.omg', function (data, status) { alert('Cross Site Request Forgery\\n\\nUpdated Email: ' + data.email + '\\nStatus: ' + status); });\n" +
                "</script>";

        log.debug("xss = ", xss);

        model.addAttribute("xssContent", xss);

        return "admin";
    }


}