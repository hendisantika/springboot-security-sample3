package com.hendisantika.springbootsecuritysample3.service;

import com.hendisantika.springbootsecuritysample3.entity.User;
import com.hendisantika.springbootsecuritysample3.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-sample3
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/09/18
 * Time: 20.45
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@Service("userDetailsService")
public class UserDetailsServiceImp implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("'" + username + "' User not found.");
        }

        org.springframework.security.core.userdetails.User.UserBuilder builder;
        builder = org.springframework.security.core.userdetails.User.withUsername(username);
        builder.disabled(!user.isEnabled());
        builder.password(user.getPassword());
        builder.authorities(new SimpleGrantedAuthority(user.getRole().getAuthority()));

        UserDetails userDetails = builder.build();
        log.debug("UserDetails = {}", userDetails);
        return userDetails;
    }
}