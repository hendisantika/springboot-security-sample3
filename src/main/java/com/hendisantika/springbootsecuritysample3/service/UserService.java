package com.hendisantika.springbootsecuritysample3.service;

import com.hendisantika.springbootsecuritysample3.entity.Role;
import com.hendisantika.springbootsecuritysample3.entity.User;
import com.hendisantika.springbootsecuritysample3.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-sample3
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/09/18
 * Time: 20.44
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@Service
public class UserService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserRepository userRepository;

    public User join(User user) {

        if (userRepository.findByUsername(user.getUsername()) != null) {
            return null;
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setEnabled(true);
        user.setRole(Role.USER);

        User newUser = userRepository.save(user);
        log.debug("newUser={}", newUser);

        return newUser;
    }

    @PreAuthorize("isAuthenticated() and ((#user.getUsername() == principal.getUsername()) or hasRole('ROLE_ADMIN'))")
    public User update(User user) {

        User foundUser = userRepository.findByUsername(user.getUsername());
        foundUser.setEmail(user.getEmail());

        User updatedUser = userRepository.save(foundUser);
        log.debug("updatedUser={}", updatedUser);

        return updatedUser;
    }

    @PreAuthorize("isAuthenticated() and ((#name == principal.getUsername()) or hasRole('ROLE_ADMIN'))")
    public User updateEmail(String name, String email) {

        User foundUser = userRepository.findByUsername(name);
        foundUser.setEmail(email);

        User updatedUser = userRepository.save(foundUser);
        log.debug("updatedUser={}", updatedUser);

        return updatedUser;
    }
}
