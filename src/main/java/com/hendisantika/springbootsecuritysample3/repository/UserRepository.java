package com.hendisantika.springbootsecuritysample3.repository;

import com.hendisantika.springbootsecuritysample3.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-sample3
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/09/18
 * Time: 20.43
 * To change this template use File | Settings | File Templates.
 */
public interface UserRepository extends JpaRepository<User, String> {

    User findByUsername(String username);
}