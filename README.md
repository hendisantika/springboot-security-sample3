# springboot-security-sample3

- Spring Boot 2 & Spring Security 5
- User Join(with @Valid), Login
- Use simple(single) Role
- Method level security (@PreAuthorize)
- XSS & CSRF TEST

NEXT
- User authentication management
  
## Reference

- http://spring.io/guides/gs/securing-web/
- http://spring.io/guides/topicals/spring-security-architecture/
- https://docs.spring.io/spring-security/site/docs/current/guides/html5/helloworld-boot.html
- https://spring.io/blog/2013/07/04/spring-security-java-config-preview-method-security/
- https://docs.spring.io/spring-security/site/docs/current/reference/html/csrf.html

## Quick Start
Pre Installed
- JDK 1.8 (or Java 10)
- Maven 3.5
- Git

Run
```
git clone https://gitlab.com/hendisantika/springboot-security-sample3.git
cd springboot-security-sample3
mvn clean spring-boot:run
```

## Test

http://localhost:8080

- id = admin
- pw = 123456

http://localhost:8080/h2-console

### Tested
- STS(Eclipse) 3.8.4
- IntelliJ IDEA 2018.2

```
//@formatter:off & //@formatter:on
eclipse : Preferences -> Java -> Code style -> Formatter -> Edit... (or New...) > Off/On Tags
IntelliJ : Preferences -> Editor -> Code Style > Formatter Control > Enable formatter markers in comments
```


## Dependency

### Spring Boot 2.0.6.RELEASE
- spring-boot-starter-web
- spring-boot-starter-security

#### Environment
- Java version: 8 Update 172 or 10.0.1
- Spring Boot version: 2.0.6
- Maven version: 3.5.2
- Lombok version: 1.18.0
- Default Encoding: UTF-8
- Default SCM : git

Home Page

![Home Page](img/home.png "Home Page")

Join Page

![Join Page](img/join.png "Join Page")

Login Page

![Login Page](img/login.png "Login Page")

XSS Page

![XSS Page](img/xss.png "XSS Page")

Admin Page

![Admin Page](img/admin.png "Admin Page")

User Page

![User Page](img/user.png "User Page")
